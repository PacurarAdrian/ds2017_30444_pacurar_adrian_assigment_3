<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
       
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
    
<style>
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input, select{
   
 	
 	border-radius: 4px;
 	 
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
</style>
<title>Dvd store admin</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100% >
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">DVD store</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		
		
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<fieldset >
	<legend><h2>New DVD</h2></legend>
	<br>

	<div id="mainWrapper">
           
   
    <form action="LoginServlet" method="post" class="form-horizontal">
        
        <div class="input-group input-sm">
            <label class="input-group-addon" for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Enter title" required>
        </div>
        <div class="input-group input-sm">
            <label class="input-group-addon" for="price">Price</label>
            <input type="text" class="form-control" id="price" name="price" placeholder="Enter price" required>
        </div>
      	<div class="input-group input-sm">
            <label class="input-group-addon" for="year">Year</label>
            <input type="text" class="form-control" id="year" name="year" placeholder="Enter year" required>
        </div>
             
        <div class="form-actions">
            <input type="submit"
                class="btn btn-block btn-primary btn-default" value="Submit">
        </div>
    </form>
</div>
          



	
	</fieldset>

	</td>
	<td width=10%> </td>
</tr>
</table>
<script>
	var paramOne ="${message}";
	if(paramOne!=null && paramOne!="" && paramOne.length!=0)
		alert(""+paramOne);
</script>
</body>
</html>