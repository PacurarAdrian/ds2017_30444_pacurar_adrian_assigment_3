package entities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Content<T extends Serializable> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private T item;
	
	public Content(T item)
	{
		this.item=item;
	
	}
	public Content(byte[] array,Class<T> clazz) throws ClassNotFoundException, IOException
	{
		this.item=this.getObject(array,clazz);
	}
	public  T getItem() {
		return item;
	}
	public void setItem(T item) {
		this.item = item;
		
	}
	public String toString()
	{
		return item.toString();
	}
	public byte[] getBytes() throws IOException
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
		  out = new ObjectOutputStream(bos);   
		  out.writeObject(item);
		  out.flush();
		  byte[] yourBytes = bos.toByteArray();
		  return yourBytes;
	
		} finally {
		  try {
		    bos.close();
		  } catch (IOException ex) {
		    // ignore close exception
			  ex.printStackTrace();
		  }
		}
		
		
	}
	public T getObject(byte[] yourBytes,Class<T> clazz) throws IOException, ClassNotFoundException
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(yourBytes);
		ObjectInput in = null;
		try {
		  in = new ObjectInputStream(bis);
		  
		  return clazz.cast(in.readObject());
		} finally {
		  try {
		    if (in != null) {
		      in.close();
		    }
		  } catch (IOException ex) {
		    // ignore close exception
		  }
		}
	}
	
}
