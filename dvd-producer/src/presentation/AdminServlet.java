package presentation;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.DVD;
import messageSender.EmitLog;




/**
 * Servlet implementation class AdminServlet
 */
@WebServlet(
        description = "Admin Servlet", 
       urlPatterns = { "/" }
       )
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */ 
    public AdminServlet() {
        super();
		
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
        rd.include(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 //get request parameters for userID and password
		DVD dvd=new DVD();
		
        dvd.setPrice(Integer.parseInt(request.getParameter("price")));
        dvd.setTitle(request.getParameter("title"));
    	dvd.setYear(Integer.parseInt(request.getParameter("year")));
		
       try{
	        	
	           EmitLog.emitLogs(dvd);
	           
	            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
	           

	            request.setAttribute("message", "Notifications sent successfully!");
	            rd.include(request, response);
		        	
	             
	        
		} catch (Exception e) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
            request.setAttribute("message", "Exception occured while trying to connect to consumer!");
            rd.include(request, response);
			e.printStackTrace();
		}
        
	}

}
