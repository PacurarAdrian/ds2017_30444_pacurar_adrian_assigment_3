package entities;

import java.io.Serializable;

public class DVD implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int year;
	private String title;
	private double price; 
	
	public DVD()
	{
		this(1999,"",0);
	}
	public DVD(int year,String title,double price)
	{
		this.year=year;
		this.title=title;
		this.price=price;
	}
	@Override
	public String toString()
	{
		return "DVD "+title+" is from year "+year+" and costs "+price;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
