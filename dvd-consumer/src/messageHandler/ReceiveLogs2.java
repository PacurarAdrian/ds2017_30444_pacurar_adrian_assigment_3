package messageHandler;

import com.rabbitmq.client.*;

import entities.Content;
import entities.DVD;
import services.ReadWriteTextFileJDK7;

import java.io.IOException;
import java.util.Arrays;

public class ReceiveLogs2 {
  private static final String EXCHANGE_NAME = "logs";
 private static final String PATH=".//";//"G:\\DS_maven\\dvd-consumer\\";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
    String queueName = channel.queueDeclare().getQueue();
    channel.queueBind(queueName, EXCHANGE_NAME, "");

    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
    //MailService mailService = new MailService("ds.laboratory1@gmail.com","ro.tuc.dsrl.ds.handson");
    Consumer consumer = new DefaultConsumer(channel) {
    	int count=0;
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope,
                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
    	  String message = null ;//= new String(body, "UTF-8");
    	  ReadWriteTextFileJDK7 fileWriter=new ReadWriteTextFileJDK7();
    	  System.out.println("\n [x] Received ");
          try {
  			Content<?> dvd=new Content<DVD>(body,DVD.class);
  			message=dvd.toString();
  			fileWriter.writeSmallTextFile(Arrays.asList(message), PATH+"message"+queueName+count+".txt");
  			//mailService.sendMail("adi.pacurarbg@gmail.com","New DVD available!",message);
			count++;
			System.out.print("'" + message + "'");
  		} catch (ClassNotFoundException e) {
  			
  			System.err.println("\ncould not create message object");
  			e.printStackTrace();
  		}
        
      }
    };
    channel.basicConsume(queueName, true, consumer);
  }
}
